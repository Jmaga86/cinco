from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.core.mail import send_mail
from .forms import ContactForm
from django.shortcuts import redirect

# Create your views here.
def contact(request):
    title = 'Contact Us'
    form = ContactForm(request.POST or None)
    if form.is_valid():
        form_email = form.cleaned_data.get("email")
        form_message = form.cleaned_data.get("message")
        form_full_name = form.cleane_data.get("full_name")
        #print email message, fullname
        subject = 'Site contact form'
        from_email = settings.EMAIL_HOS_USER
        to_email = [from_email, 'youotheremail@email.com']
        contact_message = "%s: %s via %s"%(
            form_full_name,
            form_message,
            form_email)
        some_html_message = """
        <h1>hello/<h1>
        """
        
        send_mail(subject,
                  contact_message,
                  from_email,
                  to_email,
                  html_message = some_html_message,
                  fail_silently = True)
        
    context = {
        "form": form,
        "title": title,
    }
    
    return render(request, "contact.html", context)