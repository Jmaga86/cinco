from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^news/$', 'blog.views.post_list', name='news'),
    url(r'^news/(?P<pk>[0-9]+)/$', 'blog.views.post_detail', name='post_detail'),
    url(r'^news/new/$', 'blog.views.post_new', name='post_new'),
    url(r'^news/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
]