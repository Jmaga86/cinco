from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    # Examples:
    url(r'^$', 'magent.views.home', name='home'),
    url(r'^contact/$', 'contact.views.contact', name='contact'),
    url(r'^about/$', 'magent.views.about', name='about'),
    url(r'', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^saspi/$', 'magent.views.saspi', name='saspi'),
    url(r'^manzoni/$', 'magent.views.manzoni', name='manzoni'),
    url(r'^tooling/$', 'magent.views.tooling', name='tooling'),
    url(r'^feeder_bowls/$', 'magent.views.feeder_bowls', name='feeder_bowls'),
    url(r'^hyodong/$', 'magent.views.hyodong', name='hyodong'),
    url(r'^gefra/$', 'magent.views.gefra', name='gefra'),
    url(r'^cct/$', 'magent.views.cct', name='cct'),
    url(r'^accessories/$', 'magent.views.accessories', name='accessories'),
    url(r'^used_machinery/$', 'magent.views.used_machinery', name='used_machinery'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)