from django.conf import Settings

from django.shortcuts import render

# Create your views here.
def home(request):
    
    return render(request, "home.html", {})


def about(request):


    return render(request, "about.html", {})

def saspi(request):
    
    return render(request, "saspi.html", {})

def manzoni(request):
    
    return render(request, "manzoni.html", {})
    
def tooling(request):
    
    return render(request, "tooling.html", {})

def feeder_bowls(request):
    
    return render(request, "feeder_bowls.html", {})    
    
def hyodong(request):
    
    return render(request, "hyodong.html", {})    
    
def gefra(request):
    
    return render(request, "gefra.html", {})    
    
def cct(request):
    
    return render(request, "cct.html", {})

def accessories(request):
    
    return render(request, "accessories.html", {})

def used_machinery(request):
    
    return render(request, "used_machinery.html", {})



